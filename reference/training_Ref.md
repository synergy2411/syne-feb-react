# Break Timing

- Tea Break : 11:00AM (15 mins)
- Lunch : 01:00 (45Mins)
- Tea Break : 03:30 (15 mins)

# React

- Renders the UI quickly and efficiently
- JavaScript Library to develop web apps
- Virtual DOM
- One way data binding
- Component based architecture
- Class based and functional components
- SPA Apps with react-router-dom library
- Huge ecosystem
- Light-weight
- Well integrated
- Not SEO friendly
- Use of JSX
- State Management using Redux
- Can use TypeScript
- Developed and sponsured by Facebook
- View Layer in MVC
- Project Architecture

# Atomic Design

- atom : smallest unit; eg. a button, one input field etc
- molecules : combo of atom; eg. search bar -> one input field + a button
- organism : combo of molecules; eg. navigation bar -> navigation link + Search bar
- template : combo of organism; eg. Form - personal detail, professional details
- page : refers to a complete web page

# Thinking in React way

- If the code is reusable, create the component

# npx create-react-app <app-name>

- npm i create-react-app -g
- create-react-app <appName>
- cd frontend
- npm start

-> component -> JSX -> JavaScript -> Virtual DOM -> Real DOM -> Data Models (changes) -> diffing algo ->
Real DOM -> User watch it on the UI

- npm i bootstrap

# Day 01 -

- React
- Virtual DOM, Diffing Algo, Reconciliation PRocess
- Component
  : Props
  : State > useState, this.state
- Unidirectional Data Flow - View -> action -> State -> View
- Form : Two way data binding

# Day 02

- Class based component
  : Error Boundary
  : Life cycle methods
  : Side effect code (make XHR call)
- Controlled / Uncontrolled Component
- Forms : refs
- Hooks : useState, useEffect, useContext, useReducer, useCallback, useMemo

# Side Effect : piece of code which is not the responsibility of React Library

- making XHR - fetch API, axios
- timer API
- state change
- routing

- Commit Phase
  : componentDidMount
  : componentDidUpdate
  : componentWillUnmount

- npm i json-server -g
- json-server --watch db.json

# Form Validation

- react-hook-form
- formik
- yup

# useEffect Flavours

- useEffect(callback) : callback will fire at initial rendering, as well as for subsequent re-rending
- useEffect(callback, []) (componentDidMount): callback will fire only at initial rendering; NOT for subsequent re-rendering
- useEffect(callback, [Dependencies]) (componentDidUpdate) : callback will fire at intial rendering; callback will fire whenever the mentioned dependency change
- useEffect(callback => cleanUpFn, []) : (componentWillUnmount)
  : callback will fire only once at the time of intial rendering
  : cleanUpFn will fire just before unloading the component
- useEffect(callback => cleanUpFn, [Deps]) :
  : callback fire at initial rendering
  : cleanUpFn fire just before the callback function for subsequent re-rendering
  : callback fire for each dependency change
  : cleanUpFn fire just before component is unloaded

# useReducer: Manage Complex State

- One slice of state is dependent on other slice of state
- const [state, dispatchFn] = useReducer(reducerFn, intialState)

# Day 03 / 04

- React hooks - useMemo, useCallback
- Routing v5 / v6.8
- Redux Pattern : Old way / RTK
- App Deployment

# React.memo() : Memoization

# JavaScript DataTypes

- Primitive : boolean, string, number
- Reference : object, array, function, date

# npm i react-router-dom

- json-server --watch db.json --port=3001

useHistory() : v5
useNavigate() : replaced useHistory() in v6; implementation of window.history object

history API : window.history

{
"id": "p002",
"title": "React for Beginners",
"author": "abc@test"
},
{
"id": "p001",
"title": "Mastering React",
"author": "test@test"
},
{
"id": "p003",
"title": "Awesome React",
"author": "xyz@test"
}

- Routing
  : Child Routing
  : Progammatic Navigation
  : Route Parameter
  : Query Parameter
  : Lazy Loading
