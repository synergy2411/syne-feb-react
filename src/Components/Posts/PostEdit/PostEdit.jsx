import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { createPortal } from "react-dom";
import { BounceLoader } from "react-spinners";
import classes from "./PostEdit.module.css";

const PostEdit = ({ deletePostHandler }) => {
  const { postId } = useParams();
  const navigate = useNavigate();

  const [post, setPost] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      fetch(`http://localhost:3001/posts/${postId}`)
        .then((resp) => resp.json())
        .then((data) => setPost(data))
        .catch(console.log);
    }, 1000);
  }, [postId]);

  const postDeleteHandler = () => {
    fetch(`http://localhost:3001/posts/${postId}`, {
      method: "DELETE",
    })
      .then((resp) => resp.json())
      .then((data) => {
        // navigate("/posts?changePost=true");
        deletePostHandler();
      })
      .catch(console.error);
  };

  if (post) {
    return (
      <div className={`${classes["backdrop"]}`}>
        {createPortal(
          <div className={`card ${classes["alert-dialog"]}`}>
            <div className="card-header">
              <h6 className="text-center">{post.title.toUpperCase()}</h6>
            </div>
            <div className="card-body">
              <p>{post.author}</p>
              <div className="d-flex justify-content-end">
                <button
                  className="btn btn-sm btn-outline-danger mx-3 px-3"
                  onClick={postDeleteHandler}
                >
                  Delete
                </button>
                <button
                  className="btn btn-sm btn-light px-3"
                  onClick={() => navigate("/posts")}
                >
                  Close
                </button>
              </div>
            </div>
          </div>,
          document.getElementById("overlay")
        )}
      </div>
    );
  } else {
    return <BounceLoader color="#ff45ff" size={"24"} />;
  }
};

export default PostEdit;
