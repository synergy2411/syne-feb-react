import { Component, useEffect, useState } from "react";

const FunctionBased = () => {
  const [clicks, setClicks] = useState(0);
  const [todos, setTodos] = useState([]);

  const clickHandler = () => setClicks(clicks + 1);

  useEffect(() => {
    fetch("http://localhost:3001/todos")
      .then((response) => response.json())
      .then((todos) => setTodos(todos));
  }, []);

  return (
    <div className="container">
      <h2>Class Based Component Loaded</h2>
      <button className="btn btn-primary" onClick={clickHandler}>
        Clicked {clicks}
      </button>
      <hr />

      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>{todo.label}</li>
        ))}
      </ul>
    </div>
  );
};

class ClassBased extends Component {
  constructor() {
    super();
    console.log("[CONSTRUCTOR]");
    this.state = {
      clicks: 0,
      todos: [],
    };
  }

  componentDidMount() {
    console.log("[COMPONENT DID MOUNT]");
    fetch("http://localhost:3001/todos")
      .then((response) => response.json())
      .then((todos) => this.setState({ todos }));
  }

  componentDidUpdate() {
    console.log("[COMPONENT DID UPDATE]");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("SHOULD COMPONENT UPDATE", nextState);
    console.log("NEXT PROPS : ", nextProps);
    if (nextProps.toggle) {
      return true;
    }
    // if (nextState.clicks < 5) {
    //   return true;
    // }
    return false;
  }

  componentWillUnmount() {
    console.log("[COMPONENT WILL UNMOUNT]");
  }

  clickHandler = () => {
    this.setState({ clicks: this.state.clicks + 1 });
    // this.state.clicks++;                 // NEVER EVER CHANGE THE STATE
  };

  render() {
    console.log("[RENDER]");
    return (
      <div className="container">
        <h2>Class Based Component Loaded</h2>
        <button className="btn btn-primary" onClick={this.clickHandler}>
          Clicked {this.state.clicks}
        </button>
        <hr />

        <ul>
          {this.state.todos.map((todo) => (
            <li key={todo.id}>{todo.label}</li>
          ))}
        </ul>
      </div>
    );
  }
}

export default ClassBased;
