import React, { useEffect, useState } from "react";
import { Route, Routes, useNavigate, useLocation } from "react-router-dom";
import Post from "./Post/Post";
import PostEdit from "./PostEdit/PostEdit";

const sortPosts = (isAscending, posts) => {
  if (isAscending) {
    return posts.sort((a, b) => {
      if (a.title > b.title) {
        return 1;
      } else if (a.title < b.title) {
        return -1;
      } else {
        return 0;
      }
    });
  } else {
    return posts.sort((a, b) => {
      if (a.title > b.title) {
        return -1;
      } else if (a.title < b.title) {
        return 1;
      } else {
        return 0;
      }
    });
  }
};

const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [postChanged, setPostChanged] = useState();

  const navigate = useNavigate();
  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const isAscending = query.get("order") === "asc";

  useEffect(() => {
    fetch("http://localhost:3001/posts")
      .then((resp) => resp.json())
      .then((data) => {
        setPosts(data);
      })
      .catch(console.log);
  }, [postChanged]);

  const selectedPost = (postId) => {
    navigate(`/posts/${postId}`);
  };

  const deletePostHandler = (postId) => {
    navigate("/posts");
    setPostChanged(!postChanged);
  };

  const sortedPosts = sortPosts(isAscending, posts);

  return (
    <React.Fragment>
      <h3 className="text-center">Posts App</h3>
      <div className="row mb-3">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button
              className="btn btn-dark"
              onClick={() =>
                navigate(`/posts?order=${isAscending ? "desc" : "asc"}`)
              }
            >
              Sort {isAscending ? "Decending" : "Ascending"}
            </button>
          </div>
        </div>
      </div>
      <div className="row">
        {posts.length > 0 &&
          sortedPosts.map((post) => (
            <Post key={post.id} post={post} selectedPost={selectedPost} />
          ))}
      </div>

      <hr />

      <Routes>
        <Route
          path="/:postId"
          element={<PostEdit deletePostHandler={deletePostHandler} />}
        />{" "}
        {/* /posts/p001 */}
      </Routes>
    </React.Fragment>
  );
};

export default Posts;
