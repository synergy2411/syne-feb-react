import React from "react";
import classes from "./Post.module.css";

const Post = ({ post, selectedPost }) => {
  return (
    <div className="col-4">
      <div
        className={`card ${classes["clickable"]}`}
        onClick={() => selectedPost(post.id)}
      >
        <div className="card-body">
          <h6 className="text-center">{post.title.toUpperCase()}</h6>
        </div>
      </div>
    </div>
  );
};

export default Post;
