import React, { useState, useEffect } from "react";

const UseEffectDemo = () => {
  const [toggle, setToggle] = useState(false);

  const [show, setShow] = useState(false);

  const [searchTerm, setSearchTerm] = useState("");
  const [repos, setRepos] = useState([]);

  useEffect(() => {
    let timer = null;
    if (searchTerm !== "") {
      timer = setTimeout(() => {
        fetch(`https://api.github.com/users/${searchTerm}/repos`)
          .then((resp) => resp.json())
          .then((repos) => {
            setRepos(repos);
          })
          .catch((err) => console.error(err));
      }, 1500);
      return () => {
        clearTimeout(timer);
      };
    }
  }, [searchTerm]);

  //   useEffect(() => {
  //     console.log("[USE EFFECT WORKS]");
  //     return () => {
  //       console.log("[CLEAN UP]");
  //     };
  //   }, [show]);

  //   console.log("Outside effect");

  return (
    <div className="container">
      <hr />

      <input
        type="text"
        name="search"
        value={searchTerm}
        onChange={(event) => setSearchTerm(event.target.value)}
      />

      <ul>
        {repos.map((repo) => (
          <li key={repo.id}>{repo.name}</li>
        ))}
      </ul>
      <hr />

      <button className="btn btn-primary" onClick={() => setToggle(!toggle)}>
        Toggle
      </button>

      <button className="btn btn-success" onClick={() => setShow(!show)}>
        Show
      </button>
      {toggle && <p>This element will toggle</p>}
      {show && <p>This element will show/hide</p>}
    </div>
  );
};

export default UseEffectDemo;
