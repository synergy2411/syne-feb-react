import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { BounceLoader } from "react-spinners";

import "./App.css";
import Header from "./Components/Header/Header";

const Auth = React.lazy(() => import("./Components/Auth/Auth"));
const Expenses = React.lazy(() => import("./Components/Expenses/Expenses"));
const Posts = React.lazy(() => import("./Components/Posts/Posts"));

function App() {
  return (
    <React.Fragment>
      <div className="container">
        <Header />
        <hr />
        <Suspense fallback={<BounceLoader color="#ff45ff" size={24} />}>
          <Routes>
            <Route path="home" element={<h3>Home Component</h3>} />
            <Route path="expenses" element={<Expenses />} />
            <Route path="auth" element={<Auth />} />
            <Route path="posts/*" element={<Posts />} />
          </Routes>
        </Suspense>
      </div>
    </React.Fragment>
  );
}

export default App;
