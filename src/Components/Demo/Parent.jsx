import React, { useState, useCallback, useMemo } from "react";
import Child from "./Child";

const Parent = () => {
  const [toggle, setToggle] = useState(true);
  const [show, setShow] = useState(false);

  console.log("[PARENT]");

  const demoFn = useCallback(() => console.log("The Demo Function"), []); // returns memoized callback
  const demoFnTwo = useMemo(() => () => console.log("Demo Function Two"), []);

  const numberArray = useMemo(() => [101, 102, 103, 104, 105], []);

  return (
    <div>
      <button onClick={() => setToggle(!toggle)}>Toggle</button>
      <button onClick={() => setShow(!show)}>Show</button>
      <Child
        toggle={true}
        demoFn={demoFn}
        numberArray={numberArray}
        demoFnTwo={demoFnTwo}
      />
    </div>
  );
};

export default Parent;

// useCallback(fn) is same as useMemo(() => fn)
