import React, { useState } from "react";

const ErrorBoundrayDemo = () => {
  const [value, setValue] = useState(0);

  if (value > 3) {
    throw new Error("Too high value");
  }

  const increaseValue = () => setValue(value + 1);

  return (
    <div className="container">
      <button className="btn btn-danger" onClick={increaseValue}>
        Value : {value}
      </button>
    </div>
  );
};

export default ErrorBoundrayDemo;
